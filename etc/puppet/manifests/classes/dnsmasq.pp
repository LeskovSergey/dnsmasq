class dnsmasq {
    package { 'dnsmasq':
	ensure => latest    
    }
    package { 'patch':
	ensure => latest    
    }
    file{'/etc/dnsmasq.cfg':
        require	=> Package['dnsmasq'],
    	ensure => present,
    }
    file{'dnsmasq_cfg_patch':
    	path => "/tmp/dnsmasq_cfg.patch",
        source => "puppet:///files/patch/dnsmasq_cfg.patch"
    }
    exec { "patch /etc/dnsmasq.conf -f < /tmp/dnsmasq_cfg.patch":
        require	=> [ 
    		    File['dnsmasq_cfg_patch'],
    		    File['/etc/dnsmasq.cfg'],
    		    Package['dnsmasq'],
    		    Package['patch']
    	            ],
        command => "patch /etc/dnsmasq.conf -f < /tmp/dnsmasq_cfg.patch",
        onlyif => "patch /etc/dnsmasq.conf -f < /tmp/dnsmasq_cfg.patch --dry-run",
        path    => ["/usr/bin", "/usr/sbin"]
    }
    service { 'dnsmasq':
	ensure => running,
        enable => true,
    	require => [
    		    Package['dnsmasq'],
		    Exec ['patch /etc/dnsmasq.conf -f < /tmp/dnsmasq_cfg.patch']
    		    ]
    }
}